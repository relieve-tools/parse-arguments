#include <iostream>
#include <string>

#include "parser.hpp"

class Custom : Parser {
 private:
  std::string file_;
  void Configure(const TokenList &tokens) final;

 public:
  Custom(int argc, char *argv[]) : file_("default.txt"), Parser(argc, argv) {
    Configure(tokens_);
  }
  virtual ~Custom() = default;

  auto &File() { return file_; }
};

void Custom::Configure(const TokenList &tokens) {
  // You can make your own short option if you want to.
  GetArgument(tokens, "-I", "--input", file_);
}

int main(int argc, char *argv[]) {
  std::string file("default.txt");
  unsigned sequence(100);

  // Use the parser to add arguments.
  Parser parser(argc, argv);

  // Call the GetArgument method which returns the argument.
  file = parser.GetArgument<std::string>("input");

  // Or get the argument using a parameter reference.
  parser.GetArgument("sequence", sequence);

  // Use a derived class instead might suits your needs.
  Custom custom(argc, argv);

  if (parser.ArgumentExists("help")) {
    std::cout << "Help is comming!" << std::endl;
  }

  std::cout << "file name: " << file << std::endl;
  std::cout << "sequence: " << sequence << std::endl;
  std::cout << "custom file name: " << custom.File() << std::endl;
}