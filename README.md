# Parse Arguments

This project contains an implementation to parse console arguments, provided as one simple header file.

## How Does It Work?

Well first, the only thing that you have to look at is the `header.hpp` file; it contains the complete implementation. Other files are there to showcase a simple example.

The header describes a `Parser` object, which allows you to either make use of the methods provided, or use as a bass class for your own implementation.

The `Parser` object provides you with the `GetArgument` and `ArgumentExists` methods. Both require only a name to be given which will be used to search through the argument list.

## I’m Still Lost, Can I Have Some Examples?

Of course! Have a look at the `src\project.cpp` file. It showcases the multiple ways you can use the `Parser` object. You can also run code and play with the examples by building the `cmake` project. 