#pragma once

#include <algorithm>
#include <sstream>
#include <string>
#include <vector>

class Parser {
 public:
  using TokenList = std::vector<std::string>;

 private:
  static constexpr unsigned kOptionStartPosition = 0;
  static constexpr unsigned kSpaceSize = 1;

  std::string GetShortOption(const std::string &option) const;
  std::string GetLongOption(const std::string &option) const;
  std::string GetArgument(const TokenList &tokens,
                          const std::string &option) const;
  std::string GetArgument(const TokenList &tokens, const std::string &soption,
                          const std::string &loption) const;
  bool OptionExist(const TokenList &tokens, const std::string &option) const;

 protected:
  TokenList tokens_;

  bool ArgumentExists(const TokenList &tokens, const std::string &soption,
                      const std::string &loption) const;

  template <typename T>
  T GetArgument(const TokenList &tokens, const std::string &soption,
                const std::string &loption) const;

  template <typename T>
  void GetArgument(const TokenList &tokens, const std::string &soption,
                   const std::string &loption, T &value) const;

  template <typename T>
  bool ZeroCheck(const T &value) const;

  virtual void Configure(const TokenList &tokens) {}

 public:
  Parser(int argc, char *argv[]) { ReadCommandLine(argc, argv); }
  virtual ~Parser() = default;

  TokenList ReadCommandLine(int argc, char *argv[]);

  bool ArgumentExists(const std::string &option) const;

  template <typename T>
  void GetArgument(const std::string &option, T &value) const;

  template <typename T>
  T GetArgument(const std::string &option) const;
};

template <typename T>
bool Parser::ZeroCheck(const T &value) const {
  return value == T{};
}

std::string Parser::GetShortOption(const std::string &option) const {
  return std::string("-") + option.front();
}

std::string Parser::GetLongOption(const std::string &option) const {
  return "--" + option;
}

Parser::TokenList Parser::ReadCommandLine(int argc, char *argv[]) {
  tokens_ = std::move(TokenList(argc - 1));
  for (int i = 1; i < argc; i++) {
    tokens_.at(i - 1) = std::string(argv[i]);
  }
  return tokens_;
}

std::string Parser::GetArgument(const TokenList &tokens,
                                const std::string &option) const {
  std::string word;
  bool match(false);
  for (const auto &token : tokens) {
    std::stringstream stream(token);
    while (!stream.eof()) {
      stream >> word;
      if (match) {
        return word;
      }
      match = (word == option);
    }
  }
  return "";
}

bool Parser::OptionExist(const TokenList &tokens,
                         const std::string &option) const {
  for (auto const &token : tokens) {
    if (token.find(option) != std::string::npos) {
      return true;
    }
  }
  return false;
}

bool Parser::ArgumentExists(const TokenList &tokens, const std::string &soption,
                            const std::string &loption) const {
  return OptionExist(tokens, soption) || OptionExist(tokens, loption);
}

std::string Parser::GetArgument(const TokenList &tokens,
                                const std::string &soption,
                                const std::string &loption) const {
  auto argument(GetArgument(tokens, soption));
  if (!argument.empty()) {
    return argument;
  } else {
    argument = GetArgument(tokens, loption);
    if (!argument.empty()) {
      return argument;
    }
  }
  return "";
}

template <typename T>
T Parser::GetArgument(const TokenList &tokens, const std::string &soption,
                      const std::string &loption) const {
  std::string str(GetArgument(tokens, soption, loption));
  std::stringstream stream(str);
  T value{};
  stream >> value;
  return value;
}

template <typename T>
void Parser::GetArgument(const TokenList &tokens, const std::string &soption,
                         const std::string &loption, T &value) const {
  auto result(GetArgument<T>(tokens, soption, loption));
  if (!ZeroCheck(result)) {
    value = result;
  }
}

template <typename T>
void Parser::GetArgument(const std::string &option, T &value) const {
  GetArgument<T>(tokens_, GetShortOption(option), GetLongOption(option), value);
}

bool Parser::ArgumentExists(const std::string &option) const {
  return ArgumentExists(tokens_, GetShortOption(option), GetLongOption(option));
}

template <typename T>
T Parser::GetArgument(const std::string &option) const {
  return GetArgument<T>(tokens_, GetShortOption(option), GetLongOption(option));
}